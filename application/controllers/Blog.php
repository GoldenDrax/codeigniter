<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class blog extends CI_Controller {

    public function __construct() {
            parent::__construct();
            $this->load->model('Nowy_model');
        }
        
	public function index()
	{
            $dane['tematy'] = $this->Nowy_model->pobierz('tematy');
            $this->load->view('Wyswietl_tematy', $dane);
	}
        
        public function nowy_temat() {
            if(!empty($_POST))
            {   
                $dane=array(
                    'czas' => time(),
                    'temat' => $this->input->post('temat'),
                    'tresc' => $this->input->post('tresc'),
                    'email' => $this->input->post('email'),
                );
                
                $this->Nowy_model->nowy('tematy', $dane);
            }
            
            $this->load->view('Nowy_temat');
        }
}
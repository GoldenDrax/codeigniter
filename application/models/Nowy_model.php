<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class nowy_model extends CI_Model{
    public $variable;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function nowy($tabela, $dane) {
        $this->db->insert($tabela, $dane);
    }
    
    public function pobierz($tabela) {
        $zapytanie=$this->db->get($tabela);
        
        return $zapytanie->result();
    }
}

